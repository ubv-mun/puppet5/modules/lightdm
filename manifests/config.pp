# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include lightdm::config
class lightdm::config {

  case $lightdm::config_dir {
    '/', '/etc', undef: {}
    default: {
      file { $lightdm::config_dir:
        ensure  => directory,
        owner   => 0,
        group   => 0,
        mode    => '0755',
        recurse => false,
      }
    }
  }

  if $lightdm::config_epp and $lightdm::config_template {
    fail('Cannot supply both config_epp and config_template templates for lightdm config file.')
  } elsif $lightdm::config_template {
    $config_content = template($lightdm::config_template)
  } elsif $lightdm::config_epp {
    $config_content = epp($lightdm::config_epp)
  } else {
    $config_content = epp('lightdm/lightdm.conf.epp')
  }

  file { $lightdm::config:
    ensure  => file,
    owner   => 0,
    group   => 0,
    mode    => $::lightdm::config_file_mode,
    content => $config_content,
  }
}
