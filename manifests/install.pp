# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include lightdm::install
class lightdm::install {
  if $lightdm::package_manage {
    package { $lightdm::package_name:
      ensure => $lightdm::package_ensure,
    }
  }
}
