# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include lightdm::service
class lightdm::service {
  if $lightdm::service_manage == true {
    service { 'lightdm':
      ensure     => $lightdm::service_ensure,
      enable     => $lightdm::service_enable,
      name       => $lightdm::service_name,
      provider   => $lightdm::service_provider,
      hasstatus  => true,
      hasrestart => true,
    }
  }
}
