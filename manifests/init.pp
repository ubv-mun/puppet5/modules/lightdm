# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include lightdm
class lightdm (
  Stdlib::Absolutepath           $config,
  Stdlib::Absolutepath           $config_dir,
  String                         $config_file_mode,
  Optional[String]               $config_epp,
  Optional[String]               $config_template,
  String                         $package_ensure,
  Boolean                        $package_manage,
  Array[String]                  $package_name,
  Boolean                        $service_enable,
  Enum['running', 'stopped']     $service_ensure,
  Boolean                        $service_manage,
  String                         $service_name,
  Optional[String]               $service_provider,
  #
  #[LightDM]
  #config_start_default_seat=true
  #config_greeter_user=lightdm
  #config_minimum_display_number=0
  #config_minimum_vt=7
  #config_lock_memory=true
  #config_user_authority_in_system_dir=false
  #config_guest_account_script=guest_account
  #config_logind_check_graphical=false
  #config_log_directory=/var/log/lightdm
  #config_run_directory=/var/run/lightdm
  #config_cache_directory=/var/cache/lightdm
  #config_sessions_directory=/usr/share/lightdm/sessions:/usr/share/xsessions:/usr/share/wayland_sessions
  #config_remote_sessions_directory=/usr/share/lightdm/remote_sessions
  #config_greeters_directory=/usr/share/lightdm/greeters:/usr/share/xgreeters
  #config_backup_logs=true
  #
  #[Seat:*]
  #seat_type=xlocal
  #seat_pam_service=lightdm
  #seat_pam_autologin_service=lightdm_autologin
  #seat_pam_greeter_service=lightdm_greeter
  #seat_xserver_command=X
  #seat_xmir_command=Xmir
  #seat_xserver_config=
  #seat_xserver_layout=
  #seat_xserver_allow_tcp=false
  #seat_xserver_share=true
  #seat_xserver_hostname=
  #seat_xserver_display_number=
  #seat_xdmcp_manager=
  #seat_xdmcp_port=177
  #seat_xdmcp_key=
  #seat_unity_compositor_command=unity_system_compositor
  #seat_unity_compositor_timeout=60
  #seat_greeter_session=example_gtk_gnome
  #seat_greeter_hide_users=false
  #seat_greeter_allow_guest=true
  #seat_greeter_show_manual_login=false
  #seat_greeter_show_remote_login=true
  #seat_user_session=default
  #seat_allow_user_switching=true
  #seat_allow_guest=true
  #seat_guest_session=
  #seat_session_wrapper=lightdm_session
  #seat_greeter_wrapper=
  #seat_guest_wrapper=
  #seat_display_setup_script=
  #seat_display_stopped_script=
  #seat_greeter_setup_script=
  #seat_session_setup_script=
  #seat_session_cleanup_script=
  #seat_autologin_guest=false
  Optional[String]               $seat_autologin_user,
  Optional[Integer]              $seat_autologin_user_timeout,
  #seat_autologin_in_background=false
  #seat_autologin_session=
  #seat_exit_on_failure=false
  #
) {

  contain 'lightdm::install'
  contain 'lightdm::config'
  contain 'lightdm::service'

  Class['::lightdm::install']
  -> Class['::lightdm::config']
  ~> Class['::lightdm::service']
}
